#include "PlayMode.h"
#include "Game.h"
#include "WindowUtils.h"

#include <iomanip>
#include <fstream>

using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;

const RECTF thrustAnim[]{
	{ 0,  0, 15, 16},
	{ 16, 0, 31, 16 },
	{ 32, 0, 47, 16 },
	{ 48, 0, 64, 16 },
};

const RECTF coinSpin[]
{
	{0, 0, 15, 15},
	{16, 0, 31, 15},
	{32, 0, 47, 15},
	{48, 0, 63, 15},
	{0, 16, 15, 31},
	{16, 16, 31, 31},
	{32, 16, 47, 31},
	{48, 16, 63, 31}
};


PlayMode::PlayMode(MyD3D & d3d)
	:mD3D(d3d), mPlayer(d3d), mThrust(d3d), mMissile(d3d), mCoinSpin(d3d), intro(d3d), gameover(d3d), title(d3d), gameoverTitle(d3d),
	stage1Title(d3d), stage2Title(d3d), stage3Title(d3d), EndTitle(d3d), boss(d3d), mBossMissile(d3d), enemy(d3d)
{
	initScore();
	InitBgnd();
	mpFont = new SpriteFont(&d3d.GetDevice(), L"data/fonts/comicSansMS.spritefont");
	assert(mpFont);
	initAsteroids();
	InitEnemy();
	InitPlayer();
	InitBoss();
	audio.Initialise();
	audio.GetSongMgr()->Play("menuTheme", true, false, &sfxHandle, 0.4f);
}

void PlayMode::UpdateMissile(float dTime)
{
	if (!mMissile.active && Game::sMKIn.IsPressed(VK_SPACE) && !(stage == Stages::STAGE3))
	{
		mMissile.active = true;
		mMissile.bullet.mPos = Vector2(mPlayer.mPos.x + mPlayer.GetScreenSize().x / 2.f, mPlayer.mPos.y);
		totalMissilesFired++;
		audio.GetSfxMgr()->Play("MAIMER", false, false, &sfxHandle, 0.1f);
	}
	mMissile.Update(dTime);

	if (stage == Stages::BOSS)
	{
		mBossMissile.active = true;
		if (mBossMissile.bbullet.mPos.x < 0)		//Add delay in firing here
			mBossMissile.bbullet.mPos = Vector2(550, boss.mPos.y);
		mBossMissile.Update(dTime);

		//Collision between the players missile and the boss
		if (mMissile.active && (boss.mPos - mMissile.bullet.mPos).Length() < 150)
		{
			mMissile.active = false;
			audio.GetSfxMgr()->Play("smallExplosion", false, false, &sfxHandle, 0.3f);
			bossHealth -= 50.f;
		}

		//Collision between the player and the boss' missile
		if (mBossMissile.active && (mBossMissile.bbullet.mPos - mPlayer.mPos).Length() < 35)
		{
			hasDied = true;
			mBossMissile.active = false;
			mBossMissile.bbullet.mPos = Vector2(100.f, -100.f);
			audio.GetSfxMgr()->Play("largeExplosion", false, false, &sfxHandle, 0.4f);
			return;
		}
	}
}

void PlayMode::UpdateBgnd(float dTime)
{
	//scroll the background layers
	int i = 0, j = 0, k = 0, l = 0;
	for (auto& s : mBgnd)
		s.Scroll(dTime*(i++)*SCROLL_SPEED, 0);

	for (auto& s : mBgndSG2)
		s.Scroll(dTime*(j++)*SCROLL_SPEED, 0);

	for (auto& s : mBgndSG3)
		s.Scroll(dTime*(k++)*SCROLL_SPEED, 0);

	for (auto& s : mBgndboss)
		s.Scroll(dTime*(l++)*SCROLL_SPEED, 0);
}

void PlayMode::UpdateInput(float dTime)
{
	Vector2 mouse{ Game::sMKIn.GetMousePos(false) };
	bool keypressed = Game::sMKIn.IsPressed(VK_UP) || Game::sMKIn.IsPressed(VK_DOWN) ||
		Game::sMKIn.IsPressed(VK_RIGHT) || Game::sMKIn.IsPressed(VK_LEFT);
	bool sticked = false;
	if (Game::sGamepads.IsConnected(0) &&
		(Game::sGamepads.GetState(0).leftStickX != 0 || Game::sGamepads.GetState(0).leftStickX != 0))
		sticked = true;

	if (keypressed || (mouse.Length() > VERY_SMALL) || sticked)
	{
		//move the ship around
		Vector2 pos(0, 0);
		if (Game::sMKIn.IsPressed(VK_UP))
			pos.y -= SPEED * dTime;
		else if (Game::sMKIn.IsPressed(VK_DOWN))
			pos.y += SPEED * dTime;
		if (Game::sMKIn.IsPressed(VK_RIGHT))
			pos.x += SPEED * dTime;
		else if (Game::sMKIn.IsPressed(VK_LEFT))
			pos.x -= SPEED * dTime;

		pos += mouse * MOUSE_SPEED * dTime;

		if (sticked)
		{
			DBOUT("left stick x=" << Game::sGamepads.GetState(0).leftStickX << " y=" << Game::sGamepads.GetState(0).leftStickY);
			pos.x += Game::sGamepads.GetState(0).leftStickX * PAD_SPEED * dTime;
			pos.y -= Game::sGamepads.GetState(0).leftStickY * PAD_SPEED * dTime;
		}

		//keep it within the play area
		pos += mPlayer.mPos;
		if (pos.x < mPlayArea.left)
			pos.x = mPlayArea.left;
		else if (pos.x > mPlayArea.right)
			pos.x = mPlayArea.right;
		if (pos.y < mPlayArea.top)
			pos.y = mPlayArea.top;
		else if (pos.y > mPlayArea.bottom)
			pos.y = mPlayArea.bottom;

		mPlayer.mPos = pos;
		mThrusting = GetClock() + 0.2f;
	}
}

void PlayMode::UpdateThrust(float dTime)
{
	if (mThrusting)
	{
		mThrust.mPos = mPlayer.mPos;
		mThrust.mPos.x -= 30;
		mThrust.mPos.y -= 12;
		mThrust.SetScale(Vector2(1.5f, 1.5f));
		mThrust.GetAnim().Update(dTime);
	}
}

void PlayMode::Update(float dTime)
{
	audio.Update();
	UpdateBgnd(dTime);
	mCoinSpin.GetAnim().Update(dTime);
	UpdateMissile(dTime);
	UpdateInput(dTime);

	UpdateThrust(dTime);
	updateAsteroids(dTime);
	UpdateEnemy(dTime);
}

void PlayMode::InitBoss()
{
	ID3D11ShaderResourceView *p = mD3D.GetCache().LoadTexture(&mD3D.GetDevice(), "boss.dds");
	boss.SetTex(*p);
	boss.SetScale(Vector2(0.4f, 0.4f));
	boss.origin = boss.GetTexData().dim / 2.f;
	boss.rotation = -PI / 2.f;

	boss.mPos = Vector2(700, (mPlayArea.bottom - mPlayArea.top) / 2.f);
}

void PlayMode::UpdateBoss(float dTime)
{
	//Code for boss AI goes here
	//Boss Movement
	if ((hitBottom) && (!hitTop))		//If the enemy has hit the bottom of the screen, move up to the top
	{
		if (boss.mPos.y > mPlayArea.top)
			boss.mPos.y -= SPEED * dTime;
		else if (boss.mPos.y < mPlayArea.top)
			boss.mPos.y = mPlayArea.top;
	}

	if ((!hitBottom) && (hitTop))
	{
		if (boss.mPos.y < mPlayArea.bottom)
			boss.mPos.y += SPEED * dTime;
		else if (boss.mPos.y > mPlayArea.bottom)
			boss.mPos.y = mPlayArea.bottom;
	}

	if (boss.mPos.y == mPlayArea.bottom)
	{
		hitBottom = true;
		hitTop = false;
	}

	if (boss.mPos.y == mPlayArea.top)
	{
		hitBottom = false;
		hitTop = true;
	}

	//Collision between player and boss sprites
	if ((boss.mPos - mPlayer.mPos).Length() < 150)
	{
		hasDied = true;
		audio.GetSfxMgr()->Play("largeExplosion", false, false, &sfxHandle, 0.4f);
		return;
	}
}

void PlayMode::BossModeUpdate(float dTime)
{

	audio.Update();
	//update the background
	UpdateBgnd(dTime);

	//update the input
	UpdateInput(dTime);

	//update the thrust
	UpdateThrust(dTime);

	//update the missile
	UpdateMissile(dTime);

	//Update the boss movement
	UpdateBoss(dTime);

	//update the coin spin
	mCoinSpin.GetAnim().Update(dTime);
}

void PlayMode::Render(float dTime, DirectX::SpriteBatch & batch) {
	//Changing background based on stage
	if (stage == Stages::STAGE1)
	{
		for (auto& s : mBgnd)
			s.Draw(batch);
	}
	else if (stage == Stages::STAGE2)
	{
		for (auto& s : mBgndSG2)
			s.Draw(batch);
	}
	else if (stage == Stages::STAGE3)
	{
		for (auto& s : mBgndSG3)
			s.Draw(batch);
	}
	else if (stage == Stages::BOSS)
	{
		for (auto& s : mBgndboss)
			s.Draw(batch);
	}

	if (mThrusting > GetClock())
		mThrust.Draw(batch);

	mCoinSpin.Draw(batch);
	stringstream ss;
	ss << std::setfill('0') << std::setw(4) << score;
	int w, h;
	WinUtil::Get().GetClientExtents(w, h);
	if (stage != Stages::STAGE3 && stage != Stages::BOSS)
		mpFont->DrawString(&batch, ss.str().c_str(), Vector2(w*0.86f, h*0.9f), Vector4(0, 0, 0, 1));
	else
		mpFont->DrawString(&batch, ss.str().c_str(), Vector2(w*0.86f, h*0.9f), Vector4(1, 1, 1, 1));	//Changes to white so it doesnt blend in with the background on stage 3

	DirectX::XMVECTOR color;
	color = Colors::White;
	mMissile.Render(batch);
	mBossMissile.Render(batch);
	mPlayer.Draw(batch);
	if (stage == Stages::BOSS)	boss.Draw(batch);	//**TESTING MOVE INTO ITS OWN FUNCTION
	if (stage != Stages::BOSS)	renderAsteroids(batch);
	if (stage == Stages::STAGE2 || stage == Stages::STAGE3)  RenderEnemy(batch);
}

void PlayMode::Release()
{
	delete mpFont;
	mpFont = nullptr;
}

void PlayMode::initScore()
{
	vector<RECTF> frames(coinSpin, coinSpin + sizeof(coinSpin) / sizeof(coinSpin[0]));
	ID3D11ShaderResourceView *p = mD3D.GetCache().LoadTexture(&mD3D.GetDevice(), "coin.dds", "coin", true, &frames);
	mCoinSpin.SetTex(*p);
	mCoinSpin.SetScale(Vector2(2, 2));
	mCoinSpin.GetAnim().Init(0, 7, 15, true);
	int w, h;
	WinUtil::Get().GetClientExtents(w, h);
	mCoinSpin.mPos = Vector2(w*0.8f, h*0.9f);
	mCoinSpin.GetAnim().Play(true);
}

Asteroid *PlayMode::checkCollAsteroids(Asteroid& me, int& score)
{
	assert(!mAsteroids.empty());
	float radius = mAsteroids[0].spr.GetScreenSize().Length() / 2.f;
	size_t i = 0;
	Asteroid *pColl = nullptr;
	while (i < mAsteroids.size() && !pColl)
	{
		Asteroid& collider = mAsteroids[i];
		if ((&me != &collider) && collider.active && (collider.spr.mPos - me.spr.mPos).Length() < (radius * 2))
			pColl = &mAsteroids[i];
		i++;
	}
	return pColl;
}

Asteroid* PlayMode::spawnAsteroid()
{
	assert(!mAsteroids.empty());
	size_t i = 0;
	Asteroid*p = nullptr;
	while (i < mAsteroids.size() && !p)
	{
		if (!mAsteroids[i].active)
			p = &mAsteroids[i];
		i++;
	}

	if (p)
	{
		int w, h;
		WinUtil::Get().GetClientExtents(w, h);
		float radius = mAsteroids[0].spr.GetScreenSize().Length() / 2.f;
		Vector2& pos = p->spr.mPos;
		pos.y = (float)GetRandom(radius, h - radius);
		pos.x = (float)(w + radius);
		bool collision = false;
		if (checkCollAsteroids(*p, score))
			collision = true;
		if (!collision)
			p->active = true;
		else
			p = nullptr;
	}
	return p;
}

void PlayMode::updateAsteroids(float dTime)
{
	assert(!mAsteroids.empty());
	float radius = mAsteroids[0].spr.GetScreenSize().Length() / 2.f;
	for (auto& a : mAsteroids)
		a.Update(dTime);

	if (stage == Stages::STAGE1)		//Resets the asteroid spawn rate when restarting the game
		SpawnRateSec = 0.7f;

	if (stage == Stages::STAGE2)		//Changing the asteroid spawn rate to increase difficulty for stage 2
		SpawnRateSec = 0.4f;

	if (stage == Stages::STAGE3)		//Changing the asteroid spawn rate to increase difficulty for stage 3
		SpawnRateSec = 0.15f;

	if ((GetClock() - lastSpawn) > SpawnRateSec)
	{
		if (spawnAsteroid())
			lastSpawn = GetClock();
	}
	size_t i = 0;
	while (i < mAsteroids.size())
	{
		Asteroid& collider = mAsteroids[i];

		//Making the asteroids faster to increase difficulty for each stage
		if (stage == Stages::STAGE1)	collider.asteroid_speed = 100;
		if (stage == Stages::STAGE2)	collider.asteroid_speed = 200;
		if (stage == Stages::STAGE3)		collider.asteroid_speed = 400;

		//When an asteroid has collided with a missile
		if (collider.active && (collider.spr.mPos - mMissile.bullet.mPos).Length() < 30)
		{
			score += 40;
			asteroidKillTotal++;
			if (stage == Stages::STAGE2)	STG2totalKilled++;
			mMissile.active = false;
			mMissile.bullet.mPos = Vector2(-100, 100);
			collider.active = false;
			collider.spr.mPos = Vector2(100, -100);
			audio.GetSfxMgr()->Play("smallExplosion", false, false, &sfxHandle, 0.2f);
		}

		//When the player has collided with an asteroid
		if (collider.active && (collider.spr.mPos - mPlayer.mPos).Length() < 35)
		{
			hasDied = true;
			collider.active = false;
			collider.spr.mPos = Vector2(100, -100);
			audio.GetSfxMgr()->Play("largeExplosion", false, false, &sfxHandle, 0.2f);
			return;
		}
		i++;
	}

}

void PlayMode::initAsteroids()
{
	assert(mAsteroids.empty());
	Asteroid a(mD3D);
	a.Init();
	mAsteroids.insert(mAsteroids.begin(), GC::roid_cache, a);
	for (auto& a : mAsteroids)
	{
		if (GetRandom(0, 1) == 0)
			a.spr.GetAnim().Init(0, 31, GetRandom(10.f, 20.f), true);
		else
			a.spr.GetAnim().Init(32, 63, GetRandom(10.f, 20.f), true);

		a.spr.GetAnim().SetFrame(GetRandom(a.spr.GetAnim().GetStart(), a.spr.GetAnim().GetEnd()));
	}
}

void PlayMode::renderAsteroids(SpriteBatch & batch)
{
	for (auto& a : mAsteroids)
		a.Render(batch);
}

void PlayMode::InitBgnd()
{
	//a sprite for each layer
	assert(mBgnd.empty());
	mBgnd.insert(mBgnd.begin(), BGND_LAYERS, Sprite(mD3D));

	assert(mBgndSG2.empty());
	mBgndSG2.insert(mBgndSG2.begin(), BGND_LAYERS, Sprite(mD3D));

	assert(mBgndSG3.empty());
	mBgndSG3.insert(mBgndSG3.begin(), BGND_LAYERS, Sprite(mD3D));

	assert(mBgndboss.empty());
	mBgndboss.insert(mBgndboss.begin(), BGND_LAYERS, Sprite(mD3D));

	//a neat way to package pairs of things (nicknames and filenames)
	pair<string, string> files[BGND_LAYERS]{
		{ "bgnd0","backgroundlayers/SpaceBackground.dds"},
		{"bgnd1","backgroundlayers/SpaceBackground.dds"}
	};

	//Backgrounds for Stage 2
	pair<string, string> filesSG2[BGND_LAYERS]{
		{"SG2bgnd0", "backgroundlayers/SpaceBackgroundSG2.dds"},
		{"SG2bgnd1", "backgroundlayers/SpaceBackgroundSG2.dds"}
	};

	//Backgrounds for Stage 3
	pair<string, string> filesSG3[BGND_LAYERS]{
		{"SG3bgnd0", "backgroundlayers/SpaceBackgroundSG3.dds"},
		{"SG3bgnd1", "backgroundlayers/SpaceBackgroundSG3.dds"}
	};

	//Backgrounds for boss
	pair<string, string> filesboss[BGND_LAYERS]{
		{"Bossbgnd0", "backgroundlayers/BossBackground.dds"},
		{"Bossbgnd1", "backgroundlayers/BossBackground.dds"}
	};
	int i = 0, j = 0, k = 0, l = 0;
	//Stage 1
	for (auto& f : files)
	{
		//set each texture layer
		ID3D11ShaderResourceView *p = mD3D.GetCache().LoadTexture(&mD3D.GetDevice(), f.second, f.first);
		if (!p)
			assert(false);
		mBgnd[i++].SetTex(*p);
	}

	//Stage 2
	for (auto& SGf : filesSG2)
	{
		ID3D11ShaderResourceView *p = mD3D.GetCache().LoadTexture(&mD3D.GetDevice(), SGf.second, SGf.first);
		if (!p)
			assert(false);
		mBgndSG2[j++].SetTex(*p);
	}

	//Stage 3
	for (auto& SGf3 : filesSG3)
	{
		ID3D11ShaderResourceView *p = mD3D.GetCache().LoadTexture(&mD3D.GetDevice(), SGf3.second, SGf3.first);
		if (!p)
			assert(false);
		mBgndSG3[k++].SetTex(*p);
	}

	//Boss Stage
	for (auto& boss : filesboss)
	{
		ID3D11ShaderResourceView *p = mD3D.GetCache().LoadTexture(&mD3D.GetDevice(), boss.second, boss.first);
		if (!p)
			assert(false);
		mBgndboss[l++].SetTex(*p);
	}

}

void PlayMode::InitPlayer()
{
	//load a orientate the ship
	ID3D11ShaderResourceView *p = mD3D.GetCache().LoadTexture(&mD3D.GetDevice(), "ship.dds");
	mPlayer.SetTex(*p);
	mPlayer.SetScale(Vector2(0.5f, 0.5f));
	mPlayer.origin = mPlayer.GetTexData().dim / 2.f;
	mPlayer.rotation = PI / 2.f;

	//setup the play area
	int w, h;
	WinUtil::Get().GetClientExtents(w, h);
	mPlayArea.left = mPlayer.GetScreenSize().x*0.6f;
	mPlayArea.top = mPlayer.GetScreenSize().y * 0.6f;
	mPlayArea.right = w - mPlayArea.left;
	mPlayArea.bottom = h * 0.85f;
	mPlayer.mPos = Vector2(mPlayArea.left + mPlayer.GetScreenSize().x / 2.f, (mPlayArea.bottom - mPlayArea.top) / 2.f);

	vector<RECTF> frames(thrustAnim, thrustAnim + sizeof(thrustAnim) / sizeof(thrustAnim[0]));
	p = mD3D.GetCache().LoadTexture(&mD3D.GetDevice(), "thrust.dds", "thrust", true, &frames);
	mThrust.SetTex(*p);
	mThrust.GetAnim().Init(0, 3, 15, true);
	mThrust.GetAnim().Play(true);
	mThrust.rotation = PI / 2.f;

	mMissile.Init(mD3D);
	mBossMissile.Init(mD3D);
}

void PlayMode::introMode()
{
	ID3D11ShaderResourceView* p = mD3D.GetCache().LoadTexture(&mD3D.GetDevice(), "Intro.dds", "intro", true);
	intro.SetTex(*p);
	intro.SetScale(Vector2(WinUtil::Get().GetClientWidth() / intro.GetTexData().dim.x, WinUtil::Get().GetClientHeight() / intro.GetTexData().dim.y));

	p = mD3D.GetCache().LoadTexture(&mD3D.GetDevice(), "TitleImage.dds", "title", true);
	title.SetTex(*p);
	title.SetScale(Vector2(0.75f, 0.75f));
	title.mPos = Vector2(200.f, 120.f);
}

void PlayMode::introRender(DirectX::SpriteBatch& batch)
{
	intro.Draw(batch);
	title.Draw(batch);
	stringstream txt, txt2, txt3, txt4;
	txt << "Press S to start the game";
	txt2 << "Press H to view highscore table";
	txt3 << "Press C to view the controls";
	txt4 << "Press Escape to exit the game (*SAD FACE*)";
	int w, h;
	WinUtil::Get().GetClientExtents(w, h);
	mpFont->DrawString(&batch, txt.str().c_str(), Vector2(w*0.25f, h*0.5f), Vector4(1, 1, 1, 1));
	mpFont->DrawString(&batch, txt2.str().c_str(), Vector2(w*0.25f, h*0.6f), Vector4(1, 1, 1, 1));
	mpFont->DrawString(&batch, txt3.str().c_str(), Vector2(w*0.25f, h*0.7f), Vector4(1, 1, 1, 1));
	mpFont->DrawString(&batch, txt4.str().c_str(), Vector2(w*0.25f, h*0.8f), Vector4(1, 1, 1, 1));
	DirectX::XMVECTOR color;
	color = Colors::Blue;
}

void PlayMode::controlMode()
{
	ID3D11ShaderResourceView* p = mD3D.GetCache().LoadTexture(&mD3D.GetDevice(), "Intro.dds", "intro", true);
	intro.SetTex(*p);
	intro.SetScale(Vector2(WinUtil::Get().GetClientWidth() / intro.GetTexData().dim.x, WinUtil::Get().GetClientHeight() / intro.GetTexData().dim.y));
}

void PlayMode::controlRender(DirectX::SpriteBatch& batch)
{
	intro.Draw(batch);
	stringstream txt, txt2, txt3;
	txt << "Use the arrow keys to move the ship around the screen";
	txt2 << "Use the space key to fire a missile towrds the enemies";
	txt3 << "A Gamepad can also be used with this game APPARENTLY ;)";
	int w, h;
	WinUtil::Get().GetClientExtents(w, h);
	mpFont->DrawString(&batch, txt.str().c_str(), Vector2(w*0.2f, h*0.3f), Vector4(0, 1, 1, 1));
	mpFont->DrawString(&batch, txt2.str().c_str(), Vector2(w*0.2f, h*0.4f), Vector4(0, 1, 1, 1));
	mpFont->DrawString(&batch, txt3.str().c_str(), Vector2(w*0.2f, h*0.5f), Vector4(0, 1, 1, 1));
}

void PlayMode::highScoreMode()
{
	ID3D11ShaderResourceView* p = mD3D.GetCache().LoadTexture(&mD3D.GetDevice(), "Intro.dds", "intro", true);
	intro.SetTex(*p);
	intro.SetScale(Vector2(WinUtil::Get().GetClientWidth() / intro.GetTexData().dim.x, WinUtil::Get().GetClientHeight() / intro.GetTexData().dim.y));
}

void PlayMode::highScoreRender(DirectX::SpriteBatch& batch)
{
	intro.Draw(batch);
	stringstream txt, highscoreTb;
	txt << "This is the highscore table section, look at snakes\n will do this in future";
	vector<int> scores(5);

	//Get scores from scores.txt file
	ifstream fin;
	fin.open("scores.txt", ios::out);
	if (fin.fail())
		highscoreTb << "Error loading scores";
	else
		fin >> scores.at(0) >> scores.at(1) >> scores.at(2) >> scores.at(3) >> scores.at(4);

	fin.close();

	highscoreTb << "1st : " << scores.at(0)
		<< "\n2nd : " << scores.at(1)
		<< "\n3rd : " << scores.at(2)
		<< "\n4th : " << scores.at(3)
		<< "\n5th : " << scores.at(4);
	int w, h;
	WinUtil::Get().GetClientExtents(w, h);
	mpFont->DrawString(&batch, txt.str().c_str(), Vector2(w*0.2f, h*0.3f), Vector4(1, 0, 1, 1));
	mpFont->DrawString(&batch, highscoreTb.str().c_str(), Vector2(w*0.2f, h* 0.4f), Vector4(1, 1, 1, 1));
}

void PlayMode::stage1Clear()
{
	ID3D11ShaderResourceView* p = mD3D.GetCache().LoadTexture(&mD3D.GetDevice(), "Intro.dds", "intro", true);
	intro.SetTex(*p);
	intro.SetScale(Vector2(WinUtil::Get().GetClientWidth() / intro.GetTexData().dim.x, WinUtil::Get().GetClientHeight() / intro.GetTexData().dim.y));

	p = mD3D.GetCache().LoadTexture(&mD3D.GetDevice(), "Stage1Title.dds", "stage1Title", true);
	stage1Title.SetTex(*p);
	stage1Title.SetScale(Vector2(0.4f, 0.4f));
	stage1Title.mPos = Vector2(230.f, 80.f);
}

void PlayMode::stage1ClearRender(DirectX::SpriteBatch& batch)
{
	float accuracy = (asteroidKillTotal / totalMissilesFired) * 100;

	intro.Draw(batch);
	stage1Title.Draw(batch);
	stringstream txt;
	txt << "Score: " << score << "\nAsteroids Destroyed : " << asteroidKillTotal << "\nMissiles Fired : " << totalMissilesFired << "\nAccuracy Rate: " << accuracy << " %" << "\nNext Objective: Defeat 60 enemies\nGood Luck"
		<< "\nPress S to continue to Stage 2";
	int w, h;
	WinUtil::Get().GetClientExtents(w, h);
	mpFont->DrawString(&batch, txt.str().c_str(), Vector2(w*0.3f, h*0.3f), Vector4(1, 1, 0, 1));
}

void PlayMode::stage2Clear()
{
	ID3D11ShaderResourceView* p = mD3D.GetCache().LoadTexture(&mD3D.GetDevice(), "Intro.dds", "intro", true);
	intro.SetTex(*p);
	intro.SetScale(Vector2(WinUtil::Get().GetClientWidth() / intro.GetTexData().dim.x, WinUtil::Get().GetClientHeight() / intro.GetTexData().dim.y));

	p = mD3D.GetCache().LoadTexture(&mD3D.GetDevice(), "stage2Title.dds", "stage2Title", true);
	stage2Title.SetTex(*p);
	stage2Title.SetScale(Vector2(0.4f, 0.4f));
	stage2Title.mPos = Vector2(230.f, 80.f);
}

void PlayMode::stage2ClearRender(DirectX::SpriteBatch& batch)
{
	float accuracy = (asteroidKillTotal / totalMissilesFired) * 100;
	intro.Draw(batch);
	stage2Title.Draw(batch);
	stringstream txt;
	txt << "Score: " << score << "\nAsteroids Destroyed : " << asteroidKillTotal << "\nMissiles Fired : " << totalMissilesFired << "\nAccuracy Rate: " << accuracy << " %" << "\nNext Objective: Your weapons have temporarily disabled\nSurvive without shooting\nGood Luck"
		<< "\nPress S to continue to Stage 3";
	int w, h;
	WinUtil::Get().GetClientExtents(w, h);
	mpFont->DrawString(&batch, txt.str().c_str(), Vector2(w*0.3f, h*0.3f), Vector4(1, 1, 0, 1));
}

void PlayMode::stage3Clear()
{
	ID3D11ShaderResourceView* p = mD3D.GetCache().LoadTexture(&mD3D.GetDevice(), "Intro.dds", "intro", true);
	intro.SetTex(*p);
	intro.SetScale(Vector2(WinUtil::Get().GetClientWidth() / intro.GetTexData().dim.x, WinUtil::Get().GetClientHeight() / intro.GetTexData().dim.y));

	p = mD3D.GetCache().LoadTexture(&mD3D.GetDevice(), "stage3Title.dds", "stage3Title", true);
	stage3Title.SetTex(*p);
	stage3Title.SetScale(Vector2(0.4f, 0.4f));
	stage3Title.mPos = Vector2(230.f, 80.f);
}

void PlayMode::stage3ClearRender(DirectX::SpriteBatch& batch)
{
	float accuracy = (asteroidKillTotal / totalMissilesFired) * 100;
	intro.Draw(batch);
	stage3Title.Draw(batch);
	stringstream txt;
	txt << "Score: " << score << "\nNext Objective: ??? \nGood Luck" << "\nPress S to continue to ???";
	int w, h;
	WinUtil::Get().GetClientExtents(w, h);
	mpFont->DrawString(&batch, txt.str().c_str(), Vector2(w*0.3f, h*0.3f), Vector4(1, 1, 0, 1));
}

void PlayMode::bossClear()
{
	ID3D11ShaderResourceView* p = mD3D.GetCache().LoadTexture(&mD3D.GetDevice(), "Intro.dds", "intro", true);
	intro.SetTex(*p);
	intro.SetScale(Vector2(WinUtil::Get().GetClientWidth() / intro.GetTexData().dim.x, WinUtil::Get().GetClientHeight() / intro.GetTexData().dim.y));

	p = mD3D.GetCache().LoadTexture(&mD3D.GetDevice(), "EndTitle.dds", "EndTitle", true);
	EndTitle.SetTex(*p);
	EndTitle.SetScale(Vector2(0.6f, 0.6f));
	EndTitle.mPos = Vector2(180.f, 80.f);
}

void PlayMode::bossClearRender(DirectX::SpriteBatch& batch)
{
	intro.Draw(batch);
	EndTitle.Draw(batch);
	stringstream txt;
	txt << "Your Final Score Was " << score << "\nYou saved the universe from darkness \nAnd thus life goes on\n Rest, Our Hero"
		<< "\n\nPress H to view the highscore table\nPress R to restart the game";
	int w, h;
	WinUtil::Get().GetClientExtents(w, h);
	mpFont->DrawString(&batch, txt.str().c_str(), Vector2(w*0.3f, h*0.3f), Vector4(1, 1, 0, 1));
}

void PlayMode::gameoverMode()
{
	ID3D11ShaderResourceView* p = mD3D.GetCache().LoadTexture(&mD3D.GetDevice(), "gameOver.dds", "gameover", true);
	gameover.SetTex(*p);
	gameover.SetScale(Vector2(WinUtil::Get().GetClientWidth() / gameover.GetTexData().dim.x, WinUtil::Get().GetClientHeight() / gameover.GetTexData().dim.y));

	p = mD3D.GetCache().LoadTexture(&mD3D.GetDevice(), "GameOverTitle.dds", "gameoverTitle", true);
	gameoverTitle.SetTex(*p);
	gameoverTitle.SetScale(Vector2(0.75f, 0.75f));
	gameoverTitle.mPos = Vector2(150.f, 100.f);
}

void PlayMode::gameoverRender(DirectX::SpriteBatch& batch)
{
	gameover.Draw(batch);
	gameoverTitle.Draw(batch);
	stringstream ss, txt;
	ss << "Your score was: " << std::setfill('0') << std::setw(4) << score;
	txt << "Press H to view the highscore table (which you aren't on loser)";
	int w, h;
	WinUtil::Get().GetClientExtents(w, h);
	mpFont->DrawString(&batch, ss.str().c_str(), Vector2(w*0.35f, h*0.3f), Vector4(1, 1, 1, 1));
	mpFont->DrawString(&batch, txt.str().c_str(), Vector2(w*0.1f, h*0.5f), Vector4(1, 1, 1, 1));
}

float PlayMode::getBossHealth()
{
	return bossHealth;
}

int PlayMode::getScore()
{
	return score;
}

void PlayMode::storeScore()
{

	vector<int> scores(5);
	//Stores score to txt file to be used in highscore table
	ifstream fin;
	fin.open("scores.txt", ios::out);
	if (fin.fail())
	{
		ofstream fout("scores.txt");
		if (fout.fail())
			assert(false);
		else
		{
			fout << score;
			fout.close();
		}
	}
	else
	{
		int previousScore;
		fin >> scores.at(0) >> scores.at(1) >> scores.at(2) >> scores.at(3) >> scores.at(4);
		ofstream fout("scores.txt", ios::out);
		if (fout.fail())
			assert(false);
		else
		{

			for (int i = 4; i > -1; i--)
			{
				if (score >= scores.at(i))
				{
					previousScore = scores.at(i);
					scores.at(i) = score;		//Replace score at 5th position with new score
					if (i < 4)
						scores.at(i + 1) = previousScore;
				}

			}


			fout << scores.at(0) << "\n" << scores.at(1) << "\n" << scores.at(2) << "\n" << scores.at(3) << "\n" << scores.at(4) << "\n";
			fout.close();
		}
	}
}



void PlayMode::InitEnemy()
{
	enemy.Init(mD3D);
}

void PlayMode::UpdateEnemy(float dTime)
{
	enemy.Update(dTime);

	if ((GetClock() - enemy.enemyLastSpawn) > enemy.enemySpawnRate)
	{
		if (spawnEnemy())
			enemy.enemyLastSpawn = GetClock();
	}


	//When the enemy is hit by a missile
	if (enemy.active && (enemy.enemySpr.mPos - mMissile.bullet.mPos).Length() < 45)
	{
		enemy.active = false;
		score += 100;
		asteroidKillTotal++;
		if (stage == Stages::STAGE2)	STG2totalKilled++;
		mMissile.active = false;
		mMissile.bullet.mPos = Vector2(-100, 100);
		enemy.enemySpr.mPos = Vector2(100, -100);
		audio.GetSfxMgr()->Play("smallExplosion", false, false, &sfxHandle, 0.2f);
	}

	//When the player has collided with the enemy
	if (enemy.active && (enemy.enemySpr.mPos - mPlayer.mPos).Length() < 50)
	{
		hasDied = true;
		enemy.active = false;
		enemy.enemySpr.mPos = Vector2(100, -100);
		audio.GetSfxMgr()->Play("largeExplosion", false, false, &sfxHandle, 0.2f);
		return;
	}
}

void PlayMode::RenderEnemy(DirectX::SpriteBatch& batch)
{
	enemy.Render(batch);
}

Enemy* PlayMode::spawnEnemy()
{
	Enemy*p = nullptr;
	p = &enemy;

	if (p)
	{
		int w, h;
		WinUtil::Get().GetClientExtents(w, h);
		float radius = enemy.enemySpr.GetScreenSize().Length() / 2.f;
		Vector2& pos = p->enemySpr.mPos;
		pos.y = (float)GetRandom(radius, h - radius);
		pos.x = (float)(w + radius);
		p->active = true;
	}

	return p;
}


void PlayMode::restartGame()
{
	hasDied = false;
	score = 0;
	bossHealth = 1000.f;
	asteroidKillTotal = 0;
	totalMissilesFired = 0;

	//Resets positions of Asteroids
	mAsteroids.clear();
	initAsteroids();

	//Reset positions of Enemy ships
	enemy.active = false;
	InitEnemy();

	mMissile.active = false;		//Resets any missiles on screen when player died
	mBossMissile.active = false;	//Resets any boss missiles on screen when player died
	mPlayer.mPos = Vector2(mPlayArea.left + mPlayer.GetScreenSize().x / 2.f, (mPlayArea.bottom - mPlayArea.top) / 2.f);	//Resets the position of the player when they die

}