#pragma once

#include "Sprite.h"

namespace GC
{
	const float asteroid_spawn_rate = 0.7f;		//Time between spawning next Asteroid
	const float asteroid_spawn_inc = 1;
	const float asteroid_max_spawn_delay = 4;
	const int roid_cache = 40;		//Amount of Asteroids that can be on screen at same time
}

struct Asteroid
{
	Asteroid(MyD3D& d3d)
		:spr(d3d)
	{}
	Sprite spr;
	bool active = false;	//should it render and animate?

	float asteroid_speed = 100;

	//setup
	void Init();
	/*
	draw - the atlas has two asteroids, half frames in one, half the other
	asteroids are randomly assigned one and then animate and at random fps
	*/
	void Render(DirectX::SpriteBatch& batch);
	/*
	move left until offscreen, then go inactive
	*/
	void Update(float dTime);
};