#pragma once

#include <vector>
#include <ctime>

#include "Input.h"
#include "D3D.h"
#include "SpriteBatch.h"
#include "Sprite.h"
#include "SpriteFont.h"
#include "SimpleMath.h"
#include "AudioMgrFMOD.h"

#include "Asteroid.h"
#include "Enemy.h"
#include "BossBullet.h"
#include "Bullet.h"


class PlayMode
{
public:
	PlayMode(MyD3D& d3d);
	void Update(float dTime);
	void Render(float dTime, DirectX::SpriteBatch& batch);
	void Release();

	void BossModeUpdate(float dTime);

	void introMode();
	void introRender(DirectX::SpriteBatch& batch);

	void controlMode();
	void controlRender(DirectX::SpriteBatch& batch);

	void highScoreMode();
	void highScoreRender(DirectX::SpriteBatch& batch);

	void stage1Clear();
	void stage1ClearRender(DirectX::SpriteBatch& batch);

	void stage2Clear();
	void stage2ClearRender(DirectX::SpriteBatch& batch);

	void stage3Clear();
	void stage3ClearRender(DirectX::SpriteBatch& batch);

	void bossClear();
	void bossClearRender(DirectX::SpriteBatch& batch);

	void gameoverMode();
	void gameoverRender(DirectX::SpriteBatch& batch);

	void storeScore();

	void restartGame();
	bool hasDied = false;
	enum class Stages { STAGE1, STAGE2, STAGE3, BOSS };
	Stages stage;
	float SCROLL_SPEED = 250.f;
	int STG2totalKilled = 0;

	//Stat variables
	float asteroidKillTotal = 0.f;
	float totalMissilesFired = 0.f;

	//Audio stuff
	AudioMgrFMOD audio;
	unsigned int sfxHandle;

	float getBossHealth();
	int getScore();
private:
	static const int BGND_LAYERS = 2;
	const float SPEED = 250;
	const float MOUSE_SPEED = 5000;
	const float PAD_SPEED = 500;
	int score = 0;

	MyD3D& mD3D;
	std::vector<Sprite> mBgnd; //parallax layers
	std::vector<Sprite> mBgndSG2; //Background for Stage 2
	std::vector<Sprite> mBgndSG3; //Background for Stage 3
	std::vector<Sprite> mBgndboss; //Background for BOSS
	RECTF mPlayArea;	//don't go outside this	
	Sprite mPlayer;		//jet
	Sprite mThrust;		//flames out the back
	Bullet mMissile;	//weapon, only one at once
	BossBullet mBossMissile;
	Enemy enemy;

	//Asteroid Stuff
	std::vector<Asteroid> mAsteroids;
	float SpawnRateSec = GC::asteroid_spawn_rate;
	float lastSpawn = 0;
	Asteroid* spawnAsteroid();
	void updateAsteroids(float dTime);
	void initAsteroids();
	void renderAsteroids(DirectX::SpriteBatch & batch);
	Asteroid *checkCollAsteroids(Asteroid& me, int& score);

	//Coin Stuff
	void initScore();
	Sprite mCoinSpin;
	DirectX::SpriteFont *mpFont = nullptr;

	//once we start thrusting we have to keep doing it for 
	//at least a fraction of a second or it looks whack
	float mThrusting = 0;

	//setup once
	void InitBgnd();
	void InitPlayer();
	void InitBoss();

	//Menu stuff
	Sprite intro;
	Sprite gameover;
	Sprite title;
	Sprite gameoverTitle;
	Sprite stage1Title;
	Sprite stage2Title;
	Sprite stage3Title;
	Sprite EndTitle;

	//Boss Stuff
	Sprite boss;
	void UpdateBoss(float dTime);
	bool hitTop = false, hitBottom = true;
	float bossHealth = 1000.f;

	//Enemy type 2 stuff
	void InitEnemy();
	void UpdateEnemy(float dTime);
	void RenderEnemy(DirectX::SpriteBatch & batch);
	Enemy* spawnEnemy();

	//make it move, reset it once it leaves the screen, only one at once
	void UpdateMissile(float dTime);
	//make it scroll parallax
	void UpdateBgnd(float dTime);
	//move the ship by keyboard, gamepad or mouse
	void UpdateInput(float dTime);
	//make the flames wobble when the ship moves
	void UpdateThrust(float dTime);
};