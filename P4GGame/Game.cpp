#include <iomanip>
#include <fstream>

#include "Game.h"
#include "WindowUtils.h"
#include "CommonStates.h"

using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;

MouseAndKeys Game::sMKIn;
Gamepads Game::sGamepads;



Game::Game(MyD3D& d3d)
	: mPMode(d3d), mD3D(d3d)
{
	sMKIn.Initialise(WinUtil::Get().GetMainWnd(), true, false);
	sGamepads.Initialise();
	mpSB = new SpriteBatch(&mD3D.GetDeviceCtx());
}


//any memory or resources we made need releasing at the end
void Game::Release()
{
	mPMode.Release();
	delete mpSB;
	mpSB = nullptr;
}

//called over and over, use it to update game logic
void Game::Update(float dTime, struct tm& startOfStage)
{

	//Starting the game by pressing S
	if (Game::sMKIn.IsPressed(VK_S) && (state == State::IntroMode))
	{
		mPMode.audio.GetSongMgr()->Stop();
		state = State::PLAY;
		mPMode.stage = PlayMode::Stages::STAGE1;
		now = time(0);
		startOfStage = *localtime(&now);
		startOfStageMins = startOfStage.tm_min;
		mPMode.audio.GetSongMgr()->Play("stage1", true, false, &mPMode.sfxHandle, 0.4f);
	}

	//Setting the default scroll speed for the background in case of restarting game
	if (mPMode.stage == PlayMode::Stages::STAGE1)
		mPMode.SCROLL_SPEED = 250.f;

	//Changing the scroll speed of the background/ Increases by Stage
	if (mPMode.stage == PlayMode::Stages::STAGE2)
		mPMode.SCROLL_SPEED = 1000.f;

	//Changing the scroll speed of the background for stage 3
	if (mPMode.stage == PlayMode::Stages::STAGE3)
		mPMode.SCROLL_SPEED = 4000.f;

	//Changing the scroll speed of the background for BOSS stage
	if (mPMode.stage == PlayMode::Stages::BOSS)
		mPMode.SCROLL_SPEED = 50.f;

	//TESTING CODE // **REMOVE BY END**
	if (state == State::PLAY && Game::sMKIn.IsPressed(VK_1))
	{
		state = State::Stage1ClearScreen;
		mPMode.audio.GetSongMgr()->Stop();
		mPMode.audio.GetSongMgr()->Play("inbetweenScreenTheme", true, false, &mPMode.sfxHandle, 0.4f);
	}

	if (state == State::PLAY && Game::sMKIn.IsPressed(VK_2))
	{
		mPMode.audio.GetSongMgr()->Stop();
		state = State::Stage2ClearScreen;
		mPMode.audio.GetSongMgr()->Play("inbetweenScreenTheme", true, false, &mPMode.sfxHandle, 0.4f);
	}

	if (state == State::PLAY && Game::sMKIn.IsPressed(VK_3))
	{
		state = State::Stage3ClearScreen;
		mPMode.audio.GetSongMgr()->Stop();
		mPMode.audio.GetSongMgr()->Play("inbetweenScreenTheme", true, false, &mPMode.sfxHandle, 0.4f);
	}

	if (state == State::BOSS && Game::sMKIn.IsPressed(VK_4))
	{
		state = State::BossClearScreen;
		mPMode.audio.GetSongMgr()->Stop();
		mPMode.audio.GetSongMgr()->Play("endScreen", true, false, &mPMode.sfxHandle, 0.4f);

		mPMode.storeScore();
	}
	//TESTING AREA // **REMOVE BY END**

	//Gets the total time that has passed since the player started stage 1
	if (mPMode.stage == PlayMode::Stages::STAGE1)
	{
		now = time(0);
		currentTime = *localtime(&now);
		currentMins = currentTime.tm_min;
	}

	//Objective for Stage 1 (Survive for time between range 1m 01s or 2m 00s)
	if (currentMins >= (startOfStageMins + 2) && mPMode.stage == PlayMode::Stages::STAGE1 && state == State::PLAY)
	{
        state = State::Stage1ClearScreen;
		mPMode.audio.GetSongMgr()->Stop();
		mPMode.audio.GetSongMgr()->Play("inbetweenScreenTheme", true, false, &mPMode.sfxHandle, 0.4f);
	}

	//When the player dies, goto the gameover screen
	if (mPMode.hasDied && ((state == State::PLAY) || state == State::BOSS))
	{
		mPMode.audio.GetSongMgr()->Stop();
		state = State::GOverMode;
		mPMode.audio.GetSongMgr()->Play("gameover", true, false, &mPMode.sfxHandle, 0.4f);

		mPMode.storeScore();
	}

	//When the player resets the game when they have a gameover
	if (Game::sMKIn.IsPressed(VK_R) && (state == State::GOverMode))
	{
		mPMode.restartGame();
		state = State::IntroMode;
		mPMode.audio.GetSongMgr()->Stop();
		mPMode.audio.GetSongMgr()->Play("menuTheme", true, false, &mPMode.sfxHandle, 0.4f);
	}

	//View the controls of the game
	if (Game::sMKIn.IsPressed(VK_C) && (state == State::IntroMode))
	{
		state = State::Controls;
		lastState = State::IntroMode;
	}

	//Go back to the previous visited page E.g from controls back to intro menu
	if (Game::sMKIn.IsPressed(VK_B) && !(state == State::PLAY || state == State::GOverMode || state == State::IntroMode))
		state = lastState;

	//View the highscore table from the intro screen
	if (Game::sMKIn.IsPressed(VK_H) && (state == State::IntroMode))
	{
		state = State::HighScoreTable;
		lastState = State::IntroMode;
	}

	//Viewing the highscore table from the gameover screen
	if (Game::sMKIn.IsPressed(VK_H) && (state == State::GOverMode))
	{
		state = State::HighScoreTable;
		lastState = State::GOverMode;
	}

	//Start Stage 2 when on the stage 1 clear screen
	if (Game::sMKIn.IsPressed(VK_S) && state == State::Stage1ClearScreen)
	{
		mPMode.stage = PlayMode::Stages::STAGE2;
		state = State::PLAY;
		mPMode.asteroidKillTotal = 0;
		mPMode.totalMissilesFired = 0;
		mPMode.audio.GetSongMgr()->Stop();
		mPMode.audio.GetSongMgr()->Play("stage2", true, false, &mPMode.sfxHandle, 0.4f);
	}

	//If the objective has been met in Stage 2 (Objective is 60 enemies killed)
	if (mPMode.STG2totalKilled > 59 && mPMode.stage == PlayMode::Stages::STAGE2 && state == State::PLAY)
	{
		state = State::Stage2ClearScreen;
		mPMode.audio.GetSongMgr()->Stop();
		mPMode.audio.GetSongMgr()->Play("inbetweenScreenTheme", true, false, &mPMode.sfxHandle, 0.4f);
	}

	//Start Stage 3 when on the stage 2 clear screen
	if (Game::sMKIn.IsPressed(VK_S) && state == State::Stage2ClearScreen)
	{
		mPMode.stage = PlayMode::Stages::STAGE3;
		state = State::PLAY;
		mPMode.asteroidKillTotal = 0;
		mPMode.totalMissilesFired = 0;
		now = time(0);
		startOfStage = *localtime(&now);
		startOfStageMins = startOfStage.tm_min;
		mPMode.audio.GetSongMgr()->Stop();
		mPMode.audio.GetSongMgr()->Play("stage3", true, false, &mPMode.sfxHandle, 0.4f);
	}

	//Objective for Stage 3 (Survive without shooting for range between 1m 01s or 2m 00s
	if (currentMins >= (startOfStageMins + 2) && mPMode.stage == PlayMode::Stages::STAGE3 && state == State::PLAY)
	{
		state = State::Stage3ClearScreen;
		mPMode.audio.GetSongMgr()->Stop();
		mPMode.audio.GetSongMgr()->Play("inbetweenScreenTheme", true, false, &mPMode.sfxHandle, 0.4f);
	}


	//Gets the total time that the player has been on stage 3
	if (mPMode.stage == PlayMode::Stages::STAGE3)
	{
		now = time(0);
		currentTime = *localtime(&now);
		currentMins = currentTime.tm_min;
	}

	//Starts the BOSS stage when on the stage 3 clear screen
	if (Game::sMKIn.IsPressed(VK_S) && state == State::Stage3ClearScreen)
	{
		mPMode.stage = PlayMode::Stages::BOSS;
		state = State::BOSS;
		mPMode.audio.GetSongMgr()->Stop();
		mPMode.audio.GetSongMgr()->Play("boss", true, false, &mPMode.sfxHandle, 0.4f);
	}

	//View the highscore table from the Boss clear screen
	if (Game::sMKIn.IsPressed(VK_H) && state == State::BossClearScreen)
	{
		state = State::HighScoreTable;
		lastState = State::BossClearScreen;
	}

	//Reset the game from the boss clear screen
	if (Game::sMKIn.IsPressed(VK_R) && state == State::BossClearScreen)
	{
		mPMode.restartGame();
		state = State::IntroMode;
		mPMode.audio.GetSongMgr()->Stop();
		mPMode.audio.GetSongMgr()->Play("menuTheme", true, false, &mPMode.sfxHandle, 0.4f);
	}

	//When the boss has been defeated
	if (mPMode.getBossHealth() < 0 && state == State::BOSS)
	{
		state = State::BossClearScreen;
		mPMode.audio.GetSongMgr()->Stop();
		mPMode.audio.GetSongMgr()->Play("endScreen", true, false, &mPMode.sfxHandle, 0.4f);

		mPMode.storeScore();
	}
	
	sGamepads.Update();
	switch (state)
	{
	case State::PLAY:
		mPMode.Update(dTime);
		break;
	case State::GOverMode:
		//Show game over image with score then esc to quit
		mPMode.gameoverMode();
		break;
	case State::IntroMode:
		//Show the introduction image then space to start game
		mPMode.introMode();
		break;
	case State::Controls:
		//function to render the control menu
		mPMode.controlMode();
		break;
	case State::HighScoreTable:
		//function to show the player the highscore table
		mPMode.highScoreMode();
		break;
	case State::Stage1ClearScreen:
		mPMode.stage1Clear();
		break;
	case State::Stage2ClearScreen:
		mPMode.stage2Clear();
		break;
	case State::Stage3ClearScreen:
		mPMode.stage3Clear();
		break;
	case State::BOSS:
		mPMode.BossModeUpdate(dTime);
		break;
	case State::BossClearScreen:
		mPMode.bossClear();
		break;
	}
}

//called over and over, use it to render things
void Game::Render(float dTime)
{
	mD3D.BeginRender(Colours::Black);
	
	CommonStates dxstate(&mD3D.GetDevice());
	mpSB->Begin(SpriteSortMode_Deferred, dxstate.NonPremultiplied(), &mD3D.GetWrapSampler());

	switch (state)
	{
	case State::PLAY:
		mPMode.Render(dTime, *mpSB);
		break;
	case State::IntroMode:
		mPMode.introRender(*mpSB);
		break;
	case State::Controls:
		mPMode.controlRender(*mpSB);
		break;
	case State::HighScoreTable:
		mPMode.highScoreRender(*mpSB);
		break;
	case State::GOverMode:
		mPMode.gameoverRender(*mpSB);
		break;
	case State::Stage1ClearScreen:
		mPMode.stage1ClearRender(*mpSB);
		break;
	case State::Stage2ClearScreen:
		mPMode.stage2ClearRender(*mpSB);
		break;
	case State::Stage3ClearScreen:
		mPMode.stage3ClearRender(*mpSB);
		break;
	case State::BOSS:
		mPMode.Render(dTime, *mpSB);
		break;
	case State::BossClearScreen:
		mPMode.bossClearRender(*mpSB);
		break;
	}

	mpSB->End();


	mD3D.EndRender();
	sMKIn.PostProcess();
}