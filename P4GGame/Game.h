#pragma once

#include <vector>
#include <ctime>

#include "Input.h"
#include "D3D.h"
#include "SpriteBatch.h"
#include "Sprite.h"
#include "SpriteFont.h"
#include "SimpleMath.h"
#include "AudioMgrFMOD.h"

#include "PlayMode.h"


enum class State { PLAY, IntroMode, GOverMode, Controls, HighScoreTable, Stage1ClearScreen, Stage2ClearScreen, Stage3ClearScreen, BOSS, BossClearScreen };

/*
Basic wrapper for a game
*/
class Game
{
public:
	static MouseAndKeys sMKIn;
	static Gamepads sGamepads;
	State state = State::IntroMode;
	State lastState;
	Game(MyD3D& d3d);


	void Release();
	void Update(float dTime, struct tm& startOfStage);
	void Render(float dTime);

	time_t now = time(0);
	struct tm& startOfStage = *localtime(&now);
	struct tm& currentTime = *localtime(&now);
	int currentMins;
	int startOfStageMins;

private:
	MyD3D& mD3D;
	DirectX::SpriteBatch *mpSB = nullptr;
	//not much of a game, but this is it
	PlayMode mPMode;

};


